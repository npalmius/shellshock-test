#!/bin/bash

# Runs shellshock test from https://shellshocker.net/

curl https://shellshocker.net/shellshock_test.sh | bash
